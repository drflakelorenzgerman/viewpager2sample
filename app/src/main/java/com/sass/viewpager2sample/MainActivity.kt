package com.sass.viewpager2sample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.viewpager2.widget.ViewPager2
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var pagerAdapter : PagerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val list : MutableList<String> = ArrayList()
        list.add("Folan")
        list.add("Bahman")
        list.add("Bisar")

        pagerAdapter = PagerAdapter(this,list)
        pager.adapter = pagerAdapter

        pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrollStateChanged(state: Int) {
                super.onPageScrollStateChanged(state)
                when(state)
                {
                    ViewPager2.SCROLL_STATE_IDLE->Log.d("HERE","IDLE")
                    ViewPager2.SCROLL_STATE_SETTLING->Log.d("HERE","SETTLING")
                    ViewPager2.SCROLL_STATE_DRAGGING->Log.d("HERE","DRAGGING")
                }
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                Log.d("HERE","onPageScrolled position: ${position}")
                Log.d("HERE","onPageScrolled positionOffset: ${positionOffset}")
                Log.d("HERE","onPageScrolled positionOffsetPixels: ${positionOffsetPixels}")
            }

            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                Log.d("HERE","onPageSelected ${position}")
            }
        })
        Handler().postDelayed(Runnable {
            pager.setCurrentItem(2,false)
        },1000)

        pager.setPageTransformer { page, position ->
            page.alpha = 1 - Math.abs(position)
        }

    }
}
