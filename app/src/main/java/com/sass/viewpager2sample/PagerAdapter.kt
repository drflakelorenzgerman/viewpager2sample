package com.sass.viewpager2sample

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class PagerAdapter(var context: Context,var list : List<String>)
    : RecyclerView.Adapter<PagerAdapter.VH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH
            = VH(LayoutInflater.from(context)
        .inflate(R.layout.item_layout,parent,false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    class VH (item : View): RecyclerView.ViewHolder(item){
        var txt : TextView = item.findViewById(R.id.txt)
        fun bind(str : String)
        {
            txt.text = str
        }
    }
}